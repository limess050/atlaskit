import { storiesOf } from '@kadira/storybook';
import React from 'react';
import Question from '@atlaskit/icon/glyph/question';
import Arrow from '@atlaskit/icon/glyph/arrow-right';
import Avatar from '@atlaskit/avatar';
import Lozenge from '@atlaskit/lozenge';
import Tooltip from '@atlaskit/tooltip';
import { colors, gridSize } from '@atlaskit/theme';
import Item from '../src';
import { name } from '../package.json';
import {
  DropImitation,
  GroupsWrapper,
  ItemsNarrowContainer,
} from './styled/StoryHelpers';

import ItemThemeDemo from './examples/ItemThemeDemo';

// eslint-disable-next-line react/prop-types
const Icon = () => <Question label="test question" />;

storiesOf(`${name}`, module)
  .add('simple item', () => (
    <GroupsWrapper>
      <p>This is an example of simple items with or without links</p>
      <DropImitation>
        <Item href="//atlassian.com">This link will reload this window</Item>
        <Item>This is just a standard item</Item>
        <Item href="//atlassian.com" target="_blank">This link will open in another tab</Item>
      </DropImitation>
    </GroupsWrapper>
  ))
  .add('with different themes', () => (
    <GroupsWrapper>
      <p>
        The generic Item component controls the layout of standard item content.
        The styles of this layout can be customised by providing a theme.
      </p>
      <ItemThemeDemo title="Gray with standard padding" />
      <ItemThemeDemo
        title="Purple with less padding"
        padding={gridSize() * 0.75}
        backgroundColor={colors.P75}
        secondaryTextColor={colors.P400}
        focusColor={colors.N500}
      />
      <ItemThemeDemo
        title="Dark with more padding and light text"
        padding={gridSize() * 2}
        backgroundColor={colors.N500}
        textColor={colors.N0}
        secondaryTextColor={colors.N40}
      />
    </GroupsWrapper>
  ))
  .add('simple item with icons', () => (
    <GroupsWrapper>
      <p>This is an example of items with icons</p>
      <DropImitation>
        <Item elemBefore={<Icon />}>First item</Item>
        <Item elemBefore={<Icon />}>Second item</Item>
        <Item elemBefore={<Icon />}>Third item</Item>
      </DropImitation>
    </GroupsWrapper>
  ))
  .add('simple item with avatars', () => (
    <GroupsWrapper>
      <p>This is an example of droplist items with avatars</p>
      <DropImitation>
        <Item elemBefore={<Avatar size="small" />}>First item</Item>
        <Item elemBefore={<Avatar size="small" />}>Second item</Item>
        <Item elemBefore={<Avatar size="small" />}>Third item</Item>
      </DropImitation>
    </GroupsWrapper>
  ))
  .add('simple item with additional space', () => (
    <GroupsWrapper>
      <p>This is an example of items with additional space to the right</p>
      <DropImitation>
        <Item
          elemAfter={
            <div style={{ display: 'flex', alignItems: 'center', width: '105px' }}>
              <Arrow label="" /><Lozenge appearance="success">done</Lozenge>
            </div>
          }
        >first item</Item>
        <Item
          elemAfter={
            <div style={{ display: 'flex', alignItems: 'center', width: '105px' }}>
              <Arrow label="" /><Lozenge appearance="inprogress">in progress</Lozenge>
            </div>
          }
          title="title for this long item"
        >second item with very long text that is going to be cut off</Item>
      </DropImitation>
    </GroupsWrapper>
  ))
  .add('disabled items', () => (
    <GroupsWrapper>
      <p>This is an example of disabled items</p>
      <DropImitation>
        <Item elemBefore={<Icon />} isDisabled>First item</Item>
        <Item isDisabled>Second item</Item>
        <Item href="//atlassian.com" target="_blank" isDisabled>This link will open in another tab</Item>
      </DropImitation>
    </GroupsWrapper>
  ))
  .add('hidden items', () => (
    <GroupsWrapper>
      <p>This is an example of a hidden item not being visible</p>
      <DropImitation>
        <Item description="'Second item' should not be visible">First item</Item>
        <Item isHidden>Second item</Item>
        <Item>Third item</Item>
      </DropImitation>
    </GroupsWrapper>
  ))
  .add('shouldAllowMultiLine items', () => (
    <GroupsWrapper>
      <p>This is an example of a hidden item not being visible</p>
      <DropImitation>
        <Item
          elemAfter={<Icon />}
          elemBefore={<Icon />}
        >
          This line will not break on to a new line because
          it does not have shouldAllowMultiline enabled
        </Item>
        <Item
          elemAfter={<Icon />}
          elemBefore={<Icon />}
          shouldAllowMultiline
        >
          This line will break once it reaches the end because
          it has the shouldAllowMultiline prop set which is
          really a truly great thing when you have lots of text.
        </Item>
        <Item
          elemAfter={<Icon />}
          elemBefore={<Icon />}
          shouldAllowMultiline
        >
          Prop set here but not much text
        </Item>
      </DropImitation>
    </GroupsWrapper>
  ))
  .add('with description ellipsis', () => (
    <GroupsWrapper>
      <p>This item description is really long so should be truncated.</p>
      <DropImitation>
        <Item
          description="Hello I am a super long description please truncate me before I get crazy"
        >
          The description below should truncate
        </Item>
      </DropImitation>
    </GroupsWrapper>
  ))
  .add('wrapped in a tooltip', () => (
    <GroupsWrapper>
      <p>Hover over each item to reveal tooltips:</p>
      <ItemsNarrowContainer>
        <Tooltip content="I'm a tooltip" position="right">
          <Item>Tooltip to the right</Item>
        </Tooltip>
        <Tooltip content="I'm a tooltip">
          <Item>Item wrapped in a tooltip</Item>
        </Tooltip>
      </ItemsNarrowContainer>
    </GroupsWrapper>
  ))

;
