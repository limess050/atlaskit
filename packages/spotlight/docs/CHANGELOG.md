# @atlaskit/spotlight

## 1.1.2 (2017-10-26)

* bug fix; add deprecation warning to spotlight package ([3ea2312](https://bitbucket.org/atlassian/atlaskit/commits/3ea2312))
## 1.1.1 (2017-10-22)

* bug fix; update styled-components dep and react peerDep ([6a67bf8](https://bitbucket.org/atlassian/atlaskit/commits/6a67bf8))
## 1.1.0 (2017-10-06)

* feature; spotlight allows to specific width ([ba77127](https://bitbucket.org/atlassian/atlaskit/commits/ba77127))
## 1.0.0-beta (2017-09-19)

* feature; initial release
